SRCDIR = src
BUILDDIR = build

PREFIX ?= /usr/local
INSTALLDIR ?= ${PREFIX}

all:
	mkdir -p "${BUILDDIR}"
	sed -e 's|^PREFIX=\.$$|PREFIX="${PREFIX}"|g' <"${SRCDIR}/vmctl" >"${BUILDDIR}/vmctl"
	chmod 755 "${BUILDDIR}/vmctl"
	cp "${SRCDIR}"/*.sh "${BUILDDIR}"/

install:
	install -Dm 755 "${BUILDDIR}"/vmctl "${INSTALLDIR}"/bin/vmctl
	ln -sf "${INSTALLDIR}/bin/vmctl" "${INSTALLDIR}/bin/diskctl"
	mkdir -p "${INSTALLDIR}"/lib/vmctl
	install -m 644 "${BUILDDIR}"/*.sh "${INSTALLDIR}"/lib/vmctl
