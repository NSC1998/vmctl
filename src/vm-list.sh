description='List available machines'
usage='[OPTIONS ...]'

while [ $# -gt 0 ]; do
	case "$1" in
		-h|--help) help; exit $E_SUCCESS ;;
		-*) die $E_USER 'Unknown option: %s' "$1" ;;
		*) die $E_USER "Trailing arguments: $@" ;;
	esac
	shift
done

# List VMs:
for vm_cfg in "$VMCTL_MACHINEDIR"/*.cfg; do
	test -e "$vm_cfg" || continue  # globbing * does not work in empty directory

	vm_name="$(basename "$vm_cfg" .cfg)"
	vm_set "$vm_name" || continue
	printf "\033[1m%s\033[0m" "$vm_name"
	if [ -n "$vm_machine_description" ]; then
		printf " - %s" "$vm_machine_description"
	fi
	if [ $vm_state = running ]; then
		printf " \033[1;32m[%s]\033[0m" $vm_state
	fi
	printf "\n"
done
