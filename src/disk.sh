disk_set()
{
	disk_name="$1"
	disk_error=$E_SUCCESS

	for disk_format in raw qcow2 none; do
		disk_file="$VMCTL_DISKDIR/${disk_name}.${disk_format}"
		if [ -e "$disk_file" ]; then
			break;
		fi
	done
	if [ "$disk_format" = none ]; then
		disk_error '%s: Disk does not exist' "$disk_name"
	fi
	if [ $disk_error -ne $E_SUCCESS ]; then
		return $disk_error
	fi

	disk_size="$(stat -c '%s' "$disk_file")"
}

disk_error()
{
	error "$@"
	disk_error=$E_ERROR
}
