description='Edit configuration for a machine'
usage='[OPTIONS ...] MACHINE'
EDITOR="${EDITOR:-/usr/bin/vi}"
remark="The editor is \`$EDITOR\`. Set the \$EDITOR environment variable to change."

while [ $# -gt 0 ]; do
	case "$1" in
		-h|--help) help; exit $E_SUCCESS ;;
		-*) die $E_USER 'Unknown option: %s' "$1" ;;
		*) break ;;
	esac
	shift
done

# Read VM name:
vm_name="$1"
test -n "$vm_name" || die $E_USER 'Please specify a machine name'
shift
test $# -eq 0 || die $E_USER "Trailing arguments: $@"

# Launch text editor:
vm_set "$vm_name"
$EDITOR "$vm_cfg"
