TRUE=1
FALSE=0
SUCCESS=0
FAILURE=1

# Value->String conversions:
strbool()
{
	case "$1" in
		$TRUE) echo true ;;
		$FALSE) echo false ;;
	esac
}

strbool_yesno()
{
	case "$1" in
		$TRUE) echo yes ;;
		$FALSE) echo no ;;
	esac
}

strmaybe()
{
	case "$1" in
		'') echo none ;;
		*) echo "$1" ;;
	esac
}

# String->Value tests:
test_num()
{
	case "$1" in (*[!0-9]*) return $FAILURE ;; esac
	return $SUCCESS
}

test_bool()
{
	case "$1" in (0|1|TRUE|[Tt]rue|FALSE|[Ff]alse) return $SUCCESS ;; esac
	return $FAILURE
}

# String->Value conversions:
from_bool()
{
	case "$1" in
		0|FALSE|[Ff]alse) echo $FALSE ;;
		1|TRUE|[Tt]rue) echo $TRUE ;;
		*) echo "$2" ;; # default
	esac
}

# Human-readability:
size_human()
{
	case "$1" in
		?|??|???) echo "$1" ;;
		????|?????|??????) echo "$(($1 / 1024))K" ;;
		???????|????????|?????????) echo "$(($1 / 1048576))M" ;;
		*) echo "$(($1 / 1073741824))G" ;;
	esac
}

# Help messages:
description()
{
	if [ -n "$command" ]; then
		printf "%s %s: %s\n" "$APPNAME" "$command" "$description"
	else
		printf "%s: %s\n" "$APPNAME" "$description"
	fi
}

usage()
{
	if [ -n "$command" ]; then
		printf "Usage: %s %s %s\n" "$APPNAME" "$command" "$usage"
	else
		printf "Usage: %s %s\n" "$APPNAME" "$usage"
	fi
}

helpnote()
{
	if [ -n "$command" ]; then
		printf "Run \`%s %s --help\` to get more information.\n" \
			"$APPNAME" "$command"
	else
		printf "Run \`%s help\` to get more information.\n" "$APPNAME"
	fi
}

# Default options:
options()
{
	cat <<- EOF
	  -h, --help       Display this message and exit
	EOF
}

help()
{
	description
	echo
	usage
	echo
	echo "Options:"
	options
	if [ -n "$remark" ]; then
		echo
		echo "$remark"
	fi
}

# General messages/death:
_message()
{
	_message_fd=$1
	_message_col=$2
	_message_prefix="$3"
	_message_format="$4"
	shift 4
	if [ -n "$_message_format" ]; then
		printf "\033[${_message_col}m$_message_prefix\033[0m $_message_format\n" \
			"$@" >&$_message_fd
	fi
}
warn() { _message 2 33 WARN "$@"; }
error() { _message 2 31 ERROR "$@"; }
die() {
	retval=$1; shift
	_message 2 31 FATAL "$@";
	if [ $retval -eq $E_USER ]; then
		echo >&2
		usage >&2
		helpnote >&2
	fi
	exit $retval
}
