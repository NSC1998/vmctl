description='Rename an existing machine'
usage='[OPTIONS ...] OLD NEW'

while [ $# -gt 0 ]; do
	case "$1" in
		-h|--help) help; exit $E_SUCCESS ;;
		-*) die $E_USER 'Unknown option: -%s' "$1" ;;
		*) break ;;
	esac
	shift
done

# Get old and new name:
old="$1"
test -n "$old" || die $E_USER 'Please specify a machine'
shift
new="$1"
test -n "$new" || die $E_USER 'Please specify a new name for the machine'
shift
test "$old" != "$new" || die $E_USER 'The two names are identical'
test $# -eq 0 || die $E_USER "Trailing arguments: $@"

# Check:
VM_CFG_STRICT=$TRUE
vm_set "$old"
if vm_exists "$new"; then
	die $E_ERROR '%s: Machine exists' "$new"
fi
if [ $vm_state = running ]; then
	die $E_ERROR '%s: Machine is running' "$vm_name"
fi

# Rename:
mv "$VMCTL_MACHINEDIR/${old}.cfg" "$VMCTL_MACHINEDIR/${new}.cfg"
